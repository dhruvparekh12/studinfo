from django.db import models
from django.utils import timezone
from classroom.models import User,Student,Teacher
# Create your models here.

subject_list=(('Maths', 'Maths'), ('COA', 'COA'), ('CN', 'CN'), ('AT', 'AT'), ('OS','OS'),('Python','Python'))


class Attendence(models.Model):

    subject = models.CharField(choices = subject_list, max_length = 50)
    class_date=models.DateField(default=timezone.now)
    # teacher= models.ForeignKey(Teacher,on_delete=models.CASCADE, related_name='class_mentor')
    student = models.ForeignKey(User,on_delete=models.CASCADE)
    is_present = models.BooleanField(default=False)

    def __str__(self):
        return str(self.student)

    

 
 