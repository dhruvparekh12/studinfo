from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView)

from classroom.decorators import teacher_required
from django.views.generic import TemplateView
from django.views import generic
from Attendence.forms import AttendenceForm
from Attendence.models import  Attendence
from classroom.models import  User,Student,Teacher
from datetime import datetime, date
from Attendence.models import subject_list
# Create your views here.
class AttendenceView(ListView):
    model = Attendence
    form_class = AttendenceForm
    template_name = 'attendence/report.html'
    context_object_name="studlist"

    subject = '-'
    date_from = date(2019, 1, 1)
    date_to = date.today()

    def get(self, request, *args, **kwargs):
        if self.request.GET.get('date_from'):
            self.date_from = datetime.strptime(self.request.GET.get('date_from'), "%d/%m/%Y").date()               
        if self.request.GET.get('date_to'):
            self.date_to = datetime.strptime(self.request.GET.get('date_to'), "%d/%m/%Y").date()   
        self.subject = self.request.GET.get('input_subject')   
        return super(AttendenceView, self).get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        if self.date_from == date(2019, 1, 1):
            kwargs['date_from'] = '-'
        else:
            kwargs['date_from'] = self.date_from
        kwargs['date_to'] = self.date_to
        kwargs['subject'] = self.subject
    # print(kwargs['sub_list'])
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        # return Attendence.objects.all()
        if self.subject == 'Select Subject':
            return Attendence.objects.filter(class_date__range=[self.date_from, self.date_to])
        else:
            return Attendence.objects.filter(class_date__range=[self.date_from, self.date_to], subject = self.subject)

class RecordAttendence(ListView):
    model = User
    template_name = 'attendence/stud_info.html'
    context_object_name="stud"

    def get_queryset(self):
        return User.objects.filter(is_student=True)

    def get_context_data(self, **kwargs):
        kwargs['date'] = datetime.now()
        kwargs['subject'] = self.kwargs['sub']
    # print(kwargs['sub_list'])
        return super().get_context_data(**kwargs)


class SubList(TemplateView):
    template_name='attendence/sub_list.html'

    def get_context_data(self, **kwargs):
        kwargs['sub_list'] = [ sub for (sub, sub2) in subject_list ]
        # print(kwargs['sub_list'])
        return super().get_context_data(**kwargs)

def SubmitAttendence(request, sub):
    is_present_list = request.POST.getlist('checks[]')
    print("Present list", is_present_list)
    all_students = User.objects.filter(is_student = True)
    for student in all_students:
        print(student.id)
        obj = Attendence()
        obj.student = student
        obj.subject = sub
        if str(student.id) in is_present_list:
            print("Match")
            obj.is_present = True        
        obj.save()
        # is_present = False
    return redirect('subject')
    # toSave = models.Attendence(is_present=True)
    # toSave.save()
