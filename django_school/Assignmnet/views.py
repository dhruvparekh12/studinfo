from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Count
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
# from ..decorators import student_required

from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from django.views import generic
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView)
from Assignmnet.models import  Assignment
from Assignmnet.forms import AssignmentForm
# from ..models import  Student, User

class Assgn(generic.ListView):
    model = Assignment
    # form_class = TeacherSignUpForm
    template_name = 'Assignmnet/teachassgn.html'

    def get_context_data(self, **kwargs):
        # kwargs['user_type'] = 'teacher'
        kwargs['assgn'] = Assignment.objects.all()

        # kwargs['time'] = datetime.now()
        return super().get_context_data(**kwargs)


class StudentAssgn(generic.ListView):
    model = Assignment
    # form_class = TeacherSignUpForm
    template_name = 'Assignmnet/studassgn.html'

    def get_context_data(self, **kwargs):
        # kwargs['user_type'] = 'teacher'
        kwargs['assgn'] = Assignment.objects.all()

        # kwargs['time'] = datetime.now()
        return super().get_context_data(**kwargs)


def AssignmentUpload(request):
    if request.method == 'POST':        
        form = AssignmentForm(request.POST, request.FILES)
        if form.is_valid():       
            print("Form valid") 
            form.save()
            return redirect('teachassgn')
    else:
        form = AssignmentForm()
    return render(request, 'Assignmnet/createassgn.html', {
        'form': form
    })

# def edit_item(request, pk, model, cls):
#     item = get_object_or_404(model, pk=pk)

#     if request.method == "POST":
#         form = cls(request.POST, instance=item)
#         if form.is_valid():
#             form.save()
#             return redirect('index')
#     else:
#         form = cls(instance=item)

#         return render(request, 'inv/edit_item.html', {'form': form})



# def edit_laptop(request, pk):
#     return edit_item(request, pk, Laptops, LaptopForm)

def DeleteAssgn(request, pk):

    template = 'Assignmnet/teachassgn.html'
    Assignment.objects.filter(id=pk).delete()

    items = Assignment.objects.all()

    context = {
        'assgn': items,
    }

    return render(request, template, context)
