from django.urls import include, path,re_path

from classroom.views import classroom, students, teachers
from . import views

 
urlpatterns = [
    path('teachers/dashboard/teachassgn/', views.Assgn.as_view(), name='teachassgn'),
    path('students/dashboard/studentassgn/', views.StudentAssgn.as_view(), name='studentassgn'),
    path('teachers/dashboard/teachassgn/addassignment/', views.AssignmentUpload, name='assgnform'),
    re_path(r'^assgn/delete/(?P<pk>\d+)$',views.DeleteAssgn, name="delete_assgn"),
    
]